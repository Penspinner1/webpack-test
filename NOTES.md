## Instructions

1. Open two terminals.
2. In one terminal, `cd` into backend dir.
3. Run `yarn && yarn dev`.
4. In the other terminal, `cd` into frontend dir.
5. Run `yarn && yarn start`.

## Notes

This is after about three hours of work and was running out of time.

- Implementation on Windows machine produces new lines with `\r\n`, which might conflict in Linux/Mac machines.
- There was some missing data with revenue and funding rounds and stuff, so I hardcoded it.
- Spent a lot of time configuring GraphQL Code Generator.
- If I had more time:
  - Fix styles.
  - Render more modern loading and error states.
  - Add tests using @testing-library/react.
  - Convert to CSS to CSS modules or use a library like Styled Components... maybe use a UI framework.
  - Add ability to create/edit new companies.
  - Fix TypeScript types.
