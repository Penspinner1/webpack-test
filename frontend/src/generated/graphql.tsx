import gql from 'graphql-tag'
import * as ApolloReactCommon from '@apollo/react-common'
import * as ApolloReactHooks from '@apollo/react-hooks'
export type Maybe<T> = T | null
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string
  String: string
  Boolean: boolean
  Int: number
  Float: number
  /** The `Upload` scalar type represents a file upload. */
  Upload: any
}

export enum TCacheControlScope {
  Public = 'PUBLIC',
  Private = 'PRIVATE'
}

export type TCompany = {
  _id: Scalars['String']
  name: Scalars['String']
  website: Scalars['String']
  numberOfEmployees: Scalars['Int']
  contact: TUser
}

export type TCompanyInput = {
  name: Scalars['String']
  website: Scalars['String']
  numberOfEmployees: Scalars['Int']
}

export type TMutation = {
  addCompany: TCompany
  updateCompany: TCompany
}

export type TMutationAddCompanyArgs = {
  company: TCompanyInput
  contact: TUserInput
}

export type TMutationUpdateCompanyArgs = {
  id: Scalars['String']
  company?: Maybe<TCompanyInput>
}

export type TQuery = {
  companies: Array<TCompany>
  company?: Maybe<TCompany>
  users: Array<TUser>
  user?: Maybe<TUser>
}

export type TQueryCompanyArgs = {
  id: Scalars['String']
}

export type TQueryUserArgs = {
  id: Scalars['String']
}

export type TUser = {
  _id: Scalars['String']
  name: Scalars['String']
  email: Scalars['String']
  phone: Scalars['String']
  company: TCompany
}

export type TUserInput = {
  name: Scalars['String']
  email: Scalars['String']
  phone: Scalars['String']
}

export type TCompaniesQueryVariables = {}

export type TCompaniesQuery = {
  companies: Array<
    Pick<TCompany, '_id' | 'name' | 'website' | 'numberOfEmployees'> & {
      contact: Pick<TUser, '_id' | 'name' | 'email' | 'phone'>
    }
  >
}

export type TCompanyQueryVariables = {
  id: Scalars['String']
}

export type TCompanyQuery = {
  company: Maybe<
    Pick<TCompany, 'name' | 'website' | 'numberOfEmployees'> & {
      contact: Pick<TUser, 'name' | 'email'>
    }
  >
}

export const CompaniesDocument = gql`
  query Companies {
    companies {
      _id
      name
      website
      numberOfEmployees
      contact {
        _id
        name
        email
        phone
      }
    }
  }
`

/**
 * __useCompaniesQuery__
 *
 * To run a query within a React component, call `useCompaniesQuery` and pass it any options that fit your needs.
 * When your component renders, `useCompaniesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCompaniesQuery({
 *   variables: {
 *   },
 * });
 */
export function useCompaniesQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<TCompaniesQuery, TCompaniesQueryVariables>
) {
  return ApolloReactHooks.useQuery<TCompaniesQuery, TCompaniesQueryVariables>(
    CompaniesDocument,
    baseOptions
  )
}
export function useCompaniesLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<TCompaniesQuery, TCompaniesQueryVariables>
) {
  return ApolloReactHooks.useLazyQuery<TCompaniesQuery, TCompaniesQueryVariables>(
    CompaniesDocument,
    baseOptions
  )
}
export type CompaniesQueryHookResult = ReturnType<typeof useCompaniesQuery>
export type CompaniesLazyQueryHookResult = ReturnType<typeof useCompaniesLazyQuery>
export type CompaniesQueryResult = ApolloReactCommon.QueryResult<
  TCompaniesQuery,
  TCompaniesQueryVariables
>
export const CompanyDocument = gql`
  query Company($id: String!) {
    company(id: $id) {
      name
      website
      numberOfEmployees
      contact {
        name
        email
      }
    }
  }
`

/**
 * __useCompanyQuery__
 *
 * To run a query within a React component, call `useCompanyQuery` and pass it any options that fit your needs.
 * When your component renders, `useCompanyQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCompanyQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useCompanyQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<TCompanyQuery, TCompanyQueryVariables>
) {
  return ApolloReactHooks.useQuery<TCompanyQuery, TCompanyQueryVariables>(
    CompanyDocument,
    baseOptions
  )
}
export function useCompanyLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<TCompanyQuery, TCompanyQueryVariables>
) {
  return ApolloReactHooks.useLazyQuery<TCompanyQuery, TCompanyQueryVariables>(
    CompanyDocument,
    baseOptions
  )
}
export type CompanyQueryHookResult = ReturnType<typeof useCompanyQuery>
export type CompanyLazyQueryHookResult = ReturnType<typeof useCompanyLazyQuery>
export type CompanyQueryResult = ApolloReactCommon.QueryResult<
  TCompanyQuery,
  TCompanyQueryVariables
>
