import React from 'react'

import './Container.css'

interface IContainerProps {
  onViewMore: () => void
}

const Container: React.FC<IContainerProps> = ({ children, onViewMore }) => {
  return (
    <div className="container">
      <div>{children}</div>
      <div className="viewMoreBox" onClick={onViewMore}>
        View More ->
      </div>
    </div>
  )
}

export default Container
