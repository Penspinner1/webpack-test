import React from 'react'
import { useParams, Link } from 'react-router-dom'

import { useCompanyQuery } from './generated/graphql'
import Card from './Card'
import SubText from './shared/SubText'

const CompanyDetails = () => {
  const { id } = useParams()
  const { data, error, loading } = useCompanyQuery({ variables: { id } })

  if (loading) {
    return <div>Loading...</div>
  }

  if (error) {
    console.error(error)

    return <div>Error</div>
  }

  return (
    <div>
      <Link to="/">Back</Link>
      <h2>{data.company.name} Inc.</h2>
      <div
        style={{
          display: 'grid',
          gridColumnGap: '24px',
          gridRowGap: '16px',
          gridTemplateColumns: '1fr 1fr'
        }}
      >
        <div>
          <Card
            header="Financial Overview"
            body={
              <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr' }}>
                <div>
                  <span style={{ fontWeight: 'bold', fontSize: '16px' }}>9.2M</span>
                  <SubText>last 12/m revenue</SubText>
                </div>
                <div>
                  <span style={{ fontWeight: 'bold', fontSize: '16px' }}>6-12 months</span>
                  <SubText>est. runway</SubText>
                </div>
              </div>
            }
          />
        </div>
        <div>
          <Card
            header="Funding Rounds"
            body={
              <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr 1fr' }}>
                <div style={{ textAlign: 'left' }}>
                  <div>Seed</div>
                  <div>Series A</div>
                  <div>Series B</div>
                </div>
                <div style={{ textAlign: 'center' }}>
                  <div>Nov, 2014</div>
                  <div>Jan, 2016</div>
                  <div>Sep, 2019</div>
                </div>
                <div style={{ textAlign: 'right' }}>
                  <div>1.4M</div>
                  <div>9.0M</div>
                  <div>22.0M</div>
                </div>
              </div>
            }
          />
        </div>
        <div>
          <Card
            header="Contact"
            body={
              <div>
                <div style={{ fontWeight: 'bold', fontSize: '16px' }}>
                  {data.company.contact.name}
                </div>
                <SubText>Chief Executive Officer</SubText>
                <a href={`mailto:${data.company.contact.email}`}>{data.company.contact.email}</a>
              </div>
            }
          />
        </div>
      </div>
    </div>
  )
}

export default CompanyDetails
