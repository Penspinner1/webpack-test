import React from 'react'

import { useCompaniesQuery } from './generated/graphql'
import CompanyListItem from './CompanyListItem'
import './Companies.css'

const Companies = () => {
  const { data: companiesResult, loading, error } = useCompaniesQuery()

  if (loading) {
    return <div>'Loading...'</div>
  }

  if (error) {
    console.error(error)
    return <div>'Error: '</div>
  }

  return (
    <div>
      <h2>Companies App</h2>
      {companiesResult!.companies.map(company => (
        <div className="item" key={company._id}>
          <CompanyListItem company={company} />
        </div>
      ))}
    </div>
  )
}

export default Companies
