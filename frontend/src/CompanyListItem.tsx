import React from 'react'
import { Link } from 'react-router-dom'

import { TCompaniesQuery } from './generated/graphql'
import SubText from './shared/SubText'
import './CompanyListItem.css'

interface ICompanyListItemProps {
  company: TCompaniesQuery['companies'][0]
}

const CompanyListItem = ({ company }: ICompanyListItemProps) => {
  return (
    <div className="companyListItem">
      <div className="companyListItemInfo">
        <div className="companyListItemRow">
          <span className="companyListItemName">{company.name}</span>
          <span className="companyListItemRev">9.2M</span>
        </div>
        <div className="companyListItemRow">
          <SubText>Last funding round: $22.0M on Sep, 2019</SubText>
          <SubText>12/m revenue</SubText>
        </div>
      </div>
      <Link className="companyListItemViewMore" to={`/company-details/${company._id}`}>
        View More ->
      </Link>
    </div>
  )
}

export default CompanyListItem
