import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Companies from './Companies'
import CompanyDetails from './CompanyDetails'
import './App.css'

const App = () => (
  <div className="App">
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <Companies />
        </Route>
        <Route path="/company-details/:id">
          <CompanyDetails />
        </Route>
      </Switch>
    </BrowserRouter>
  </div>
)

export default App
