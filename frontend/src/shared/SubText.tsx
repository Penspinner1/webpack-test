import React from 'react'

import './SubText.css'

interface ISubTextProps {
  as?: React.ComponentType | keyof React.ReactHTML
}

const SubText: React.FC<ISubTextProps> = ({ as: Component = 'div', children }) => (
  <Component className="subtext">{children}</Component>
)

export default SubText
