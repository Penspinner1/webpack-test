import React from 'react'

import './Card.css'

interface ICardProps {
  header: React.ReactNode
  body: React.ReactNode
}

const Card = ({ header, body }: ICardProps) => (
  <div className="card">
    <div className="cardHeader">{header}</div>
    <div className="cardBody">{body}</div>
  </div>
)

export default Card
